package fer.diplab.backend.persistenceTests;

import fer.diplab.backend.repository.LicensePhotoRepository;
import fer.diplab.backend.model.DriveStatus;
import fer.diplab.backend.model.LicensePhoto;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.TestAbortedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

@SpringBootTest
public class LicensePhotoTest {

    private Binary binaryImage;
    @Autowired
    private LicensePhotoRepository repository;

    @BeforeEach
    public void test_mongo() {
        BufferedImage img;
        try {
            img = ImageIO.read(new File("C:/Users/Korisnik/Pictures/Stipe.jpg"));
        } catch (IOException e) {
            throw new TestAbortedException("This test requires valid path to image.");
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            ImageIO.write(img, "jpg", baos);
            baos.flush();
            this.binaryImage = new Binary(BsonBinarySubType.BINARY, baos.toByteArray());
            baos.close();
        } catch (IOException e) {
            throw new TestAbortedException("Unsuccessful casting image to binary!");
        }
    }

    @Test
    void insertLicensePhoto() {
        LicensePhoto l = new LicensePhoto();
        l.setPhoto(binaryImage);
        l.setStatus(DriveStatus.ENTRY);
        l.setTime(LocalDateTime.now());
        l.setCameraId(2L);
        l.setLicenseRecognizedText("Stipe");
        Assertions.assertNotNull(repository.save(l).getId());
    }
}
