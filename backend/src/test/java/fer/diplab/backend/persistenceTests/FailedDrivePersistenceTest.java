package fer.diplab.backend.persistenceTests;

import fer.diplab.backend.repository.CameraRepository;
import fer.diplab.backend.repository.FailedDriveRepository;
import fer.diplab.backend.repository.SectionCheckRepository;
import fer.diplab.backend.repository.TollboothRepository;
import fer.diplab.backend.model.Camera;
import fer.diplab.backend.model.FailedDrive;
import fer.diplab.backend.model.Tollbooth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;

@SpringBootTest
public class FailedDrivePersistenceTest {

    @Autowired
    private TollboothRepository tollboothRepository;
    @Autowired
    private FailedDriveRepository repository;
    @Autowired
    private CameraRepository cameraRepository;

    @Test
    void insertFailedDrive() {
        Tollbooth t = new Tollbooth();
        t.setLocationName("Zagreb");
        t.setTollboothName("B1");
        Camera c = new Camera();
        cameraRepository.save(c);
        ArrayList<Camera> l = new ArrayList<>();
        l.add(c);
        t.setCameras(l);
        tollboothRepository.save(t);
        FailedDrive d = new FailedDrive();
        d.setExitTime(LocalDateTime.now());
        d.setExitLocation(t);
        d.setPhotoId("aoidhq9jdpojaspc'3kjfposadkf");
        long id = repository.save(d).getId();

        Assertions.assertTrue(repository.findById(id).isPresent());
    }
}
