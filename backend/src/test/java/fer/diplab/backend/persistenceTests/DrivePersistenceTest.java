package fer.diplab.backend.persistenceTests;

import fer.diplab.backend.repository.DriveRepository;
import fer.diplab.backend.repository.TollboothRepository;
import fer.diplab.backend.model.Drive;
import fer.diplab.backend.model.Tollbooth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
public class DrivePersistenceTest {

    @Autowired
    private DriveRepository repository;

    @Autowired
    private TollboothRepository tollboothRepository;

    @Test
    void insertTollbooth() {
        Tollbooth t = new Tollbooth();
        t.setTollboothName("cab 2");
        t.setTollboothName("Ulaz Lučko");
        t.setLocationName("Zagreb");
        long id = tollboothRepository.save(t).getId();

        Assertions.assertTrue(tollboothRepository.findById(id).isPresent());
    }

    @Test
    void insertDrive() {
        Drive d = new Drive();
        Tollbooth t = new Tollbooth();
        t.setTollboothName("cab 3");
        t.setTollboothName("Ulaz Lučko");
        t.setLocationName("Zagreb");
        tollboothRepository.save(t);

        d.setLicensePlate("ZG 12323 MB");
        d.setEntryPhotoId("iojd9wqjkalskclasidj");
        d.setEntryTime(LocalDateTime.now());
        d.setEntryTollbooth(t);
        long id = repository.save(d).getId();

        Assertions.assertTrue(repository.findById(id).isPresent());
    }
}
