package fer.diplab.backend.serviceTests;

import fer.diplab.backend.repository.DriveRepository;
import fer.diplab.backend.repository.TollboothRepository;
import fer.diplab.backend.model.Drive;
import fer.diplab.backend.model.DriveStatus;
import fer.diplab.backend.model.LicensePhoto;
import fer.diplab.backend.model.Tollbooth;
import fer.diplab.backend.service.DriveService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
public class DriveServiceTest {

    @Autowired
    private DriveService driveService;
    @Autowired
    private TollboothRepository tollboothRepository;
    @Autowired
    private DriveRepository driveRepository;

    private Tollbooth tollbooth;
    private LicensePhoto photo;
    private Drive drive;
    private String licensePlate = "ZG123LL";


    @BeforeEach
    void setUp() {
        tollbooth = new Tollbooth();
        tollbooth.setTollboothName("cab 3");
        tollbooth.setTollboothName("Ulaz Lučko");
        tollbooth.setLocationName("Zagreb");
        tollboothRepository.save(tollbooth);
        photo = new LicensePhoto();
        photo.setLicenseRecognizedText("ZG2234LD");
        photo.setId("asnpajspofdjačsldkjšaoskdčlaksd");

        drive = new Drive();
        drive.setLicensePlate(licensePlate);
        drive.setEntryTollbooth(tollbooth);
        drive.setEntryPhotoId("aojdoijoaisjdlkasjd");
        drive.setEntryTime(LocalDateTime.now());
        driveRepository.save(drive);
    }

    @Test
    void createDriveTest() {
        Long id = driveService.createDrive(photo, tollbooth);
        Assertions.assertNotNull(id);
    }

    @Test
    void finishDriveTest_correctRecognizedLicensePlate() {
        LicensePhoto p = new LicensePhoto();
        p.setId("qweqwe21qweqwe");
        p.setLicenseRecognizedText(licensePlate);
        DriveStatus status = driveService.finishDrive(p, tollbooth);
        Assertions.assertEquals(status, DriveStatus.EXIT);
    }

}
