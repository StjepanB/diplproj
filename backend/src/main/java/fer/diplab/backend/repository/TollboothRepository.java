package fer.diplab.backend.repository;

import fer.diplab.backend.model.Tollbooth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TollboothRepository extends JpaRepository<Tollbooth,Long> {

    Tollbooth findByTollboothName(String tollboothName);
}
