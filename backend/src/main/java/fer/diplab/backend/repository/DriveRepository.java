package fer.diplab.backend.repository;

import fer.diplab.backend.model.Drive;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriveRepository extends JpaRepository<Drive,Long> {
    Drive findByLicensePlate(String entryTollbooth);
}
