package fer.diplab.backend.repository;

import fer.diplab.backend.model.SectionCheck;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectionCheckRepository extends JpaRepository<SectionCheck,Long> {
}
