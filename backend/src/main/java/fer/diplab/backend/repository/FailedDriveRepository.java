package fer.diplab.backend.repository;

import fer.diplab.backend.model.FailedDrive;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FailedDriveRepository extends JpaRepository<FailedDrive,Long> {
}
