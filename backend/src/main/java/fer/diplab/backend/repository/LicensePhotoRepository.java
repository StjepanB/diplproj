package fer.diplab.backend.repository;

import fer.diplab.backend.model.LicensePhoto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LicensePhotoRepository extends MongoRepository<LicensePhoto,String> {
}
