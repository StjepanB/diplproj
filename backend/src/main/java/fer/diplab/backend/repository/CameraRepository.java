package fer.diplab.backend.repository;

import fer.diplab.backend.model.Camera;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CameraRepository extends JpaRepository<Camera,Long> {
}
