package fer.diplab.backend.dto;

import org.springframework.web.multipart.MultipartFile;

public class DriveDto {

    private String tollboothName;
    private String cameraName;
    private String licensePlateText;

    public DriveDto(String tollboothName, String licensePlateText,String cameraName) {
        this.tollboothName = tollboothName;
        this.licensePlateText = licensePlateText;
        this.cameraName = cameraName;
    }


    public String getTollboothName() {
        return tollboothName;
    }

    public void setTollboothName(String tollboothName) {
        this.tollboothName = tollboothName;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public String getLicensePlateText() {
        return licensePlateText;
    }

    public void setLicensePlateText(String licensePlateText) {
        this.licensePlateText = licensePlateText;
    }
}
