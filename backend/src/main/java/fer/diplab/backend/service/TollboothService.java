package fer.diplab.backend.service;

import fer.diplab.backend.repository.TollboothRepository;
import fer.diplab.backend.model.Camera;
import fer.diplab.backend.model.Tollbooth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TollboothService {

    private TollboothRepository repository;

    @Autowired
    public TollboothService(TollboothRepository repository) {
        this.repository = repository;
    }

    public long addTollbooth(String locationName,String tollboothName, List<Camera> cameras){
        Tollbooth tollbooth = new Tollbooth();
        tollbooth.setCameras(cameras);
        tollbooth.setLocationName(locationName);
        tollbooth.setTollboothName(tollboothName);

        return repository.save(tollbooth).getId();
    }

    public long getTollboothId(String tollboothName){
        return repository.findByTollboothName(tollboothName).getId();
    }
}
