package fer.diplab.backend.service;

import fer.diplab.backend.repository.LicensePhotoRepository;
import fer.diplab.backend.model.DriveStatus;
import fer.diplab.backend.model.LicensePhoto;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class LicensePhotoService {

    private LicensePhotoRepository repository;

    @Autowired
    public LicensePhotoService(LicensePhotoRepository repository) {
        this.repository = repository;
    }

    public void addPhoto(Binary photo, DriveStatus status, String licensePlateText, long tollboothId) {
        LicensePhoto licensePhoto = new LicensePhoto();
        licensePhoto.setStatus(status);
        licensePhoto.setLicenseRecognizedText(licensePlateText);
        licensePhoto.setCameraId(tollboothId);
        licensePhoto.setTime(LocalDateTime.now());
        licensePhoto.setPhoto(photo);

        repository.save(licensePhoto);
    }


}
