package fer.diplab.backend.service;

import fer.diplab.backend.repository.DriveRepository;
import fer.diplab.backend.repository.FailedDriveRepository;
import fer.diplab.backend.repository.LicensePhotoRepository;
import fer.diplab.backend.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class DriveService {

    private DriveRepository driveRepository;
    private FailedDriveRepository failedDriveRepository;
    private LicensePhotoRepository photoRepository;

    @Autowired
    public DriveService(DriveRepository driveRepository, FailedDriveRepository failedDriveRepository, LicensePhotoRepository photoRepository) {
        this.driveRepository = driveRepository;
        this.failedDriveRepository = failedDriveRepository;
        this.photoRepository = photoRepository;
    }

    public Long createDrive(LicensePhoto photo, Tollbooth entryTollbooth) {
        Drive drive = new Drive();
        drive.setEntryTollbooth(entryTollbooth);
        drive.setEntryTime(LocalDateTime.now());
        drive.setEntryPhotoId(photo.getId());
        drive.setEntryTollbooth(entryTollbooth);
        drive.setLicensePlate(photo.getLicenseRecognizedText());

        return driveRepository.save(drive).getId();
    }

    public DriveStatus finishDrive(LicensePhoto photo, Tollbooth exitTollbooth) {
        Drive drive = driveRepository.findByLicensePlate(photo.getLicenseRecognizedText());

        if (drive == null) {
            FailedDrive failedDrive = new FailedDrive();
            failedDrive.setPhotoId(photo.getId());
            failedDrive.setExitLocation(exitTollbooth);
            failedDrive.setExitTime(LocalDateTime.now());
            failedDriveRepository.save(failedDrive);
            photo.setStatus(DriveStatus.FAILED);
            photoRepository.save(photo);
            return DriveStatus.FAILED;
        } else {
            drive.setExitLocation(exitTollbooth);
            drive.setExitPhotoId(photo.getId());
            driveRepository.save(drive);
            photo.setStatus(DriveStatus.EXIT);
            photoRepository.save(photo);
            return DriveStatus.EXIT;
        }

    }
}
