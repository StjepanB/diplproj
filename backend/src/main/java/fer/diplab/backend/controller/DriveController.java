package fer.diplab.backend.controller;

import fer.diplab.backend.dto.DriveDto;
import fer.diplab.backend.model.DriveStatus;
import fer.diplab.backend.service.DriveService;
import fer.diplab.backend.service.LicensePhotoService;
import fer.diplab.backend.service.TollboothService;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class DriveController {


    private TollboothService tollboothService;
    private DriveService driveService;
    private LicensePhotoService photoService;

    @Autowired
    public DriveController(DriveService driveService, TollboothService tollboothService, LicensePhotoService photoService) {
        this.driveService = driveService;
        this.photoService = photoService;
        this.tollboothService = tollboothService;

    }

    @PostMapping(name = "/drive")
    public ResponseEntity createDrive(@RequestParam("drive") DriveDto driveDto, @RequestParam("photo") MultipartFile photo) {
        Binary binaryPhoto;
        try {
            binaryPhoto = new Binary(photo.getBytes());
        } catch (IOException e) {
            return ResponseEntity.badRequest().build();
        }

        long tollboothId = tollboothService.getTollboothId(driveDto.getTollboothName());
        photoService.addPhoto(binaryPhoto, DriveStatus.ENTRY, driveDto.getLicensePlateText(), tollboothId);
        return ResponseEntity.ok().build();
    }
}
