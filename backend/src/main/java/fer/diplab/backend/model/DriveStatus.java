package fer.diplab.backend.model;

public enum DriveStatus {
        ENTRY,DRIVING, EXIT, FAILED
}
