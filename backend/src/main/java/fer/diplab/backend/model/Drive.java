package fer.diplab.backend.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Drive {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="drive_id")
    private long id;
    @Column(name="entry_time")
    private LocalDateTime entryTime;
    @OneToOne
    private Tollbooth entryTollbooth;
    @Column(name="entry_photo_id")
    private String entryPhotoId;
    @Column(name="license_plate")
    private String licensePlate;
    @Column(name="exit_time")
    private LocalDateTime exitTime;
    @OneToOne
    private Tollbooth exitTollbooth;
    @Column(name="exit_photo_id")
    private String exitPhotoId;

    @OneToMany
    private List<SectionCheck> sections;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(LocalDateTime entryTime) {
        this.entryTime = entryTime;
    }

    public Tollbooth getEntryLocation() {
        return entryTollbooth;
    }

    public void setEntryTollbooth(Tollbooth entryTollbooth) {
        this.entryTollbooth = entryTollbooth;
    }

    public String getEntryPhotoId() {
        return entryPhotoId;
    }

    public void setEntryPhotoId(String entryPhotoId) {
        this.entryPhotoId = entryPhotoId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String entryLicensePlate) {
        this.licensePlate = entryLicensePlate;
    }

    public LocalDateTime getExitTime() {
        return exitTime;
    }

    public void setExitTime(LocalDateTime exitTime) {
        this.exitTime = exitTime;
    }

    public Tollbooth getExitTollbooth() {
        return exitTollbooth;
    }

    public void setExitLocation(Tollbooth exitTollbooth) {
        this.exitTollbooth = exitTollbooth;
    }

    public String getExitPhotoId() {
        return exitPhotoId;
    }

    public void setExitPhotoId(String exitPhotoId) {
        this.exitPhotoId = exitPhotoId;
    }

    public Tollbooth getEntryTollbooth() {
        return entryTollbooth;
    }

    public void setExitTollbooth(Tollbooth exitTollbooth) {
        this.exitTollbooth = exitTollbooth;
    }

    public List<SectionCheck> getSections() {
        return sections;
    }

    public void setSections(List<SectionCheck> sections) {
        this.sections = sections;
    }
}
