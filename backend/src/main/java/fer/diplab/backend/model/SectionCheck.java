package fer.diplab.backend.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class SectionCheck {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "selection_check_id")
    private long id;
    private String locationName;
    @OneToMany
    private List<Camera> cameras;
    @ElementCollection
    @CollectionTable(name="photoIds", joinColumns=@JoinColumn(name="selection_check_id"))
    @Column(name="mongoId")
    private List<String> photoIds;
    private LocalDateTime passingTime;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public List<Camera> getCameras() {
        return cameras;
    }

    public void setCameras(List<Camera> cameras) {
        this.cameras = cameras;
    }

    public List<String> getPhotoIds() {
        return photoIds;
    }

    public void setPhotoIds(List<String> photoIds) {
        this.photoIds = photoIds;
    }

    public LocalDateTime getPassingTime() {
        return passingTime;
    }

    public void setPassingTime(LocalDateTime passingTime) {
        this.passingTime = passingTime;
    }
}
