package fer.diplab.backend.model;

import org.bson.types.Binary;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.time.LocalDateTime;

@Document
public class LicensePhoto {

    @Id
    private String id;
    private long cameraId;
    private DriveStatus status;
    private LocalDateTime time;
    private Binary photo;
    private String licenseRecognizedText;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    public DriveStatus getStatus() {
        return status;
    }

    public void setStatus(DriveStatus status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Binary getPhoto() {
        return photo;
    }

    public void setPhoto(Binary photo) {
        this.photo = photo;
    }

    public String getLicenseRecognizedText() {
        return licenseRecognizedText;
    }

    public void setLicenseRecognizedText(String licenseRecognizedText) {
        this.licenseRecognizedText = licenseRecognizedText;
    }
}
