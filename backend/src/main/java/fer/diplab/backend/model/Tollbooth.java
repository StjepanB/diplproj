package fer.diplab.backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Tollbooth {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String locationName;
    private String tollboothName;
    @OneToMany
    private List<Camera> cameras;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getTollboothName() {
        return tollboothName;
    }

    public void setTollboothName(String cabineName) {
        this.tollboothName = cabineName;
    }

    public List<Camera> getCameras() {
        return cameras;
    }

    public void setCameras(List<Camera> cameras) {
        this.cameras = cameras;
    }
}
