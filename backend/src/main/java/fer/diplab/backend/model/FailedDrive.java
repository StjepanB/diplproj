package fer.diplab.backend.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class FailedDrive {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String photoId;
    private String licensePlate;
    private LocalDateTime exitTime;
    @OneToOne
    private Tollbooth exitTollbooth;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public LocalDateTime getExitTime() {
        return exitTime;
    }

    public void setExitTime(LocalDateTime exitTime) {
        this.exitTime = exitTime;
    }

    public Tollbooth getExitLocation() {
        return exitTollbooth;
    }

    public void setExitLocation(Tollbooth exitTollbooth) {
        this.exitTollbooth = exitTollbooth;
    }
}
