# DiplProj

## ER diagram relacijske baze (MS SqlServer)

Tollbooth predstavlja naplatnu postaju odnosno ulaznu točku na cestu. Sadrži ime, lokaciju (jer na jednoj lokaciji ih može biti više) i te popis kamera. Na jednoj postaji može biti jedna ili više
kamera. Osim postaje sustav poznaje i dionice ceste, odnosno mjesta na kojima se prati promet. Zapise s dionice spremamo u tablicu section check, tako da možemo pratiti
kretanje vozila duž cijele rute i smanjiti nejasnoće u slučaju pogreške sustava. Naime za praćenje vožnje pojedinog vozila imamo tablicu drive koja sadrži informacije 
za jednu vožnju koju neki korisnik započinje ulaskom na cestu, svim sekcijama na kojima je uspješno pročitana tablica i izlazna točka s ceste. Na ulazu, izlazu i svim 
točkama dionice kamere fotografiraju registracijske tablice vozila koje onda algoritam pokušava pročitati. Nadalje sustav pohranjuje svaku fotografiju s pripadajućim
podacima. Na izlazu sa ceste ukoliko je točno pročitana tablica i na ulazu i na izlazu, vožnja je uspješno završena. 

TODO: Što ako vozilo nije registrirano na ulazu??? Što ako nije na izlazu?


<img src=https://gitlab.com/StjepanB/diplproj/raw/master/backend/src/main/resources/picture/diagram_baze.PNG>

## Prikaz dokumenata u NoSql bazi (MongoDB)

U dokument bazu podataka pohranjujemo svaku zabilježenu fotografiju i pripadajuće podatake (mjesto, status, vrijeme) kako bi naknadno mogli raditi analizu tih podataka
bez da vadimo te informacije iz relacijske baze podataka. Budući da se slike veliki dokumenti koji će trošiti puno memorije, prihvatljivo nam je imati redundanciju u 
sustavu.

```json
_id: "5e0baa1a9685ab08b9c53377"
cameraId:2
status:"ENTRY"
time:"2019-12-31T20:05:46.448+00:00"
photo:"Binary('/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAx...', 0)"
_class:"fer.diplab.backend.model.LicensePhoto"
```